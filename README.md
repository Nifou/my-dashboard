# A not-so-much-configurable personal dashboard

### Features:
- Display random note from your Markdown documentation directory
- Display weather at the location of your choice without needing an API key
- Display some bookmarks with icons (go to https://icones.netlify.app to see their names)
- Display TODOs from Logseq or a scallop clock (based on Android 12's) if Logseq is not
launched
- Display XKCD
- Random beautiful backgrounds from [Unsplash](https://unsplash.com) using [Lorem Picsum](https://picsum.photos).

### Getting started

- First, install Rust (using [Rustup](https://rustup.rs))
- Run `cargo install --path .`
- In a directory of your choice, run `my-dashboard`, it will create a file
  `config.yml` and a directory `cache`
- Edit the `config.yml` file as you want then restart `my-dashboard`

Note: a path to the configuration file can also be passed as the first argument

If you want to automatically start the server at startup, you can use the
systemd service in the `systemd` directory. Use the script
`systemd/make-service.sh` to enable it. The configuration file will be
`~/.config/my-dashboard/config.yml`. Once you have edited it, you can restart
the server using `systemctl --user restart my-dashboard.service`.

### License

This project is licensed under the [Mozilla Public License (MPL) 2.0](https://www.mozilla.org/en-US/MPL/2.0).
