#!/usr/bin/env sh

set -o errexit
set -o nounset

# Create required config directories
mkdir -p ~/.config/my-dashboard
mkdir -p ~/.config/systemd/user

# Copy systemd unit file
cp systemd/my-dashboard.service ~/.config/systemd/user

# Enable and start the systemd service
systemctl --user daemon-reload
systemctl --user enable my-dashboard.service
systemctl --user start my-dashboard.service

# Print a success message
if [ "$(systemctl --user is-enabled my-dashboard.service)" = "enabled" ]; then
  echo "==> my-dashboard service successfully enabled! <=="
  echo "You can now edit the configuration file at ~/.config/my-dashboard/config.yml and restart the server using systemctl --user restart my-dashboard.service"
else
  echo "Failed to register the my-dashboard service"
fi
