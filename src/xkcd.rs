use reqwest::Client;
use serde::{Serialize, Deserialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct XkcdInfo {
    month: String,
    year: String,
    day: String,
    alt: String,
    title: String,
    img: String,
}

impl XkcdInfo {
    pub async fn fetch(client: &mut Client) -> Self {
        client.get("https://xkcd.com/info.0.json").send().await.expect("failed to send XKCD request").json().await.expect("failed to deserialize XKCD response JSON")
    }
}
