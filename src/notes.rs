use chrono::{DateTime, Local, NaiveDateTime, Utc};
use comrak::{markdown_to_html, ComrakOptions};
use rand::seq::SliceRandom;
use std::{
    fs,
    time::UNIX_EPOCH,
};
use walkdir::WalkDir;

pub fn get_random_note(notes_dir: &str) -> (String, String, String) {
    let pages = WalkDir::new(notes_dir)
        .follow_links(true)
        .into_iter()
        .filter_map(|entry| entry.ok())
        .map(|entry| entry.path().to_owned())
        .filter(|path| {
            path.extension().map_or(false, |ext| ext == "md")
                && !path.to_str().map_or(false, |p| p.contains("bak"))
                && !path.to_str().map_or(false, |p| p.contains(".recycle"))
        })
        .collect::<Vec<_>>();
    let mut rng = rand::thread_rng();
    let random_page = pages
        .choose(&mut rng)
        .expect("failed to randomly choose a Markdown file, at least one is required");
    let page_content = fs::read_to_string(random_page).expect("faild to read random Markdown file");

    let modified = fs::metadata(random_page)
        .expect("failed to get metadata about random Markdown file")
        .modified()
        .expect("failed to get last modified time of random Markdown file")
        .duration_since(UNIX_EPOCH)
        .expect("system time before Unix epoch");
    let modified_naive =
        NaiveDateTime::from_timestamp_opt(modified.as_secs() as i64, modified.subsec_nanos())
            .unwrap();
    let modified_local: DateTime<Local> =
        DateTime::<Utc>::from_utc(modified_naive, Utc).with_timezone(&Local);

    (
        random_page
            .to_str()
            .unwrap()
            .strip_prefix(notes_dir)
            .unwrap()
            .to_string(),
        markdown_to_html(&page_content, &ComrakOptions::default()),
        modified_local.format("%A, %d %B %Y").to_string(),
    )
}
