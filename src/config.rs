use papero::pochoir::lang::IntoValue;
use serde::{Serialize, Deserialize};

use papero::pochoir as pochoir;

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, IntoValue)]
pub struct Bookmark {
    name: String,
    icon: String,
    link: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Config {
    #[serde(rename = "server-addr")]
    pub server_addr: String,

    #[serde(rename = "cache-dir")]
    pub cache_dir: String,

    #[serde(rename = "notes-dir")]
    pub notes_dir: String,

    #[serde(rename = "weather-city")]
    pub weather_city: usize,
    pub bookmarks: Vec<Bookmark>,

    #[serde(rename = "logseq-port")]
    pub logseq_port: Option<usize>,

    #[serde(rename = "logseq-token")]
    pub logseq_token: Option<String>,
}
