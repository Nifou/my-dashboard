use papero::pochoir::lang::IntoValue;
use reqwest::{header, Client};
use serde::{Deserialize, Serialize};

use papero::pochoir;

#[derive(Debug, Serialize, Deserialize, IntoValue)]
pub struct EntityID {
    id: usize,
}

#[derive(Debug, Serialize, Deserialize, IntoValue)]
pub struct BlockEntity {
    children: Option<Vec<BlockEntity>>,
    file: Option<EntityID>,

    id: usize,
    uuid: String,
    content: String,
}

pub async fn get_todos_page(client: &mut Client, logseq_port: usize, logseq_token: String) -> Option<Vec<BlockEntity>> {
    let req = client.post(format!(r#"http://localhost:{logseq_port}/api"#))
        .header(header::CONTENT_TYPE, "application/json")
        .bearer_auth(logseq_token)
        .body(r#"{ "method": "logseq.DB.datascriptQuery", "args": ["[:find (pull ?b [*]) :where [?b :block/marker ?marker] [(contains? #{\"NOW\" \"LATER\" \"TODO\" \"DOING\"} ?marker)] [?b :block/page ?p] (not [?b :block/scheduled]) (not [?b :block/deadline])  ]"] }"#);
    let pages: Vec<Vec<BlockEntity>> = req
        .send()
        .await
        .ok()?
        .json()
        .await
        .unwrap_or_else(|e| panic!("failed to deserialize logseq response JSON: {e}"));

    Some(pages.into_iter().flatten().collect())
}
