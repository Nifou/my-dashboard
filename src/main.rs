use chrono::{DateTime, Local, Timelike};
use encre_css::toml;
use papero::{
    log::info,
    pochoir::lang::{object, Value, IntoValue},
    server::{http::{response::Builder, StatusCode}, Handler, Request, Router, Server},
    template::Template,
    Config, Context, Stream,
};
use reqwest::{header, Proxy};
use serde::{Serialize, Deserialize};
use std::{fs, path::Path, time::Duration, net::SocketAddr, borrow::Cow, str::FromStr};

use crate::xkcd::XkcdInfo;

mod config;
mod logseq;
mod weather;
mod xkcd;
mod notes;

// Embed template in binary in release mode
#[cfg(not(debug_assertions))]
static TEMPLATE: &str = include_str!("templates/index.html");

#[tokio::main]
async fn main() -> std::io::Result<()> {
    papero::logging::init();

    let mut args = std::env::args();
    let config_path = args.nth(1).unwrap_or_else(|| "config.yml".to_string());

    // Prepare clients used to make requests
    let mut client = reqwest::Client::builder()
        .proxy(Proxy::all("socks5h://127.0.0.1:9050").expect("failed to build proxy"))
        .build()
        .expect("failed to build reqwest client");
    let local_client = reqwest::Client::builder()
        .build()
        .expect("failed to build reqwest client");

    // Read configuration file
    let config: config::Config = if Path::exists(Path::new(&config_path)) {
        info!("Read configuration from {config_path}");
        let config_file = fs::read_to_string(&config_path).expect("failed to read config file");
        serde_yaml::from_str(&config_file).unwrap_or_else(|e| panic!("failed to parse YAML config file: {e}"))
    } else {
        info!("Creating default configuration");
        let yml = "# The address on which the server listen, use 0.0.0.0 to access it from any device in your local network
server-addr: 127.0.0.1:4325

# Path to the directory where files which need to be cached will be stored
cache-dir: cache

# Path to the directory where your Markdown notes are stored
notes-dir: ~/Documents

# Current city ID, go to https://openweathermap.org to find it
weather-city: 2988507

# Some bookmark links you want to display
# The full list of icons can be searched using https://icones.netlify.app
bookmarks:
  - name: Wikipedia
    icon: mdi-wikipedia
    link: https://wikipedia.org

# Optional: If you use LogSeq, a list of your current todos can be displayed.
# You need to start the API server in LogSeq's UI and to generate a token.
# The token can be generated using `openssl rand -hex 16` and manually entered in the UI.
# Otherwise, a clock will be displayed.
# logseq-port: 12315
# logseq-token: my-logseq-token";
        fs::write(&config_path, yml).expect("failed to write default configuration file");
        serde_yaml::from_str(yml).unwrap_or_else(|e| panic!("failed to parse YAML config file: {e}"))
    };

    let cache_dir = shellexpand::tilde(&config.cache_dir);
    let notes_dir = shellexpand::tilde(&config.notes_dir).to_string();

    // Make sure the cache directory is created
    if !Path::exists(Path::new(&*cache_dir)) {
        fs::create_dir_all(&*cache_dir).expect("failed to create data directory");
    }

    // Weather
    let weather_path = Path::new(&*cache_dir).join("weather.json");
    let (weather_svg_icon, weather_raw_data) = if !Path::exists(&weather_path)
        || fs::metadata(&weather_path)
            .expect("failed to get metadata for weather data file")
            .modified()
            .expect("modified time should be available")
            .elapsed()
            .expect("failed to get elapsed time")
            > Duration::from_secs(3600)
    {
        info!("Fetching weather");
        let weather_raw_data = weather::WeatherData::fetch(&mut client, config.weather_city).await;
        let weather_svg_icon = weather_raw_data.weather[0].generate_svg_icon();
        fs::write(
            weather_path,
            serde_json::to_string(&weather_raw_data)
                .expect("failed to serialize weather data to JSON"),
        )
        .expect("failed to write weather data file");

        (weather_svg_icon, weather_raw_data)
    } else {
        let weather_raw_data: weather::WeatherData = serde_json::from_str(
            &fs::read_to_string(weather_path).expect("failed to read weather data"),
        )
        .expect("failed to deserialize weather data");
        let weather_svg_icon = weather_raw_data.weather[0].generate_svg_icon();

        (weather_svg_icon, weather_raw_data)
    };
    let weather_data = papero::pochoir::lang::serialize_to_value(weather_raw_data)
        .expect("failed to serialize weather data to value");

    // XKCD
    let xkcd_path = Path::new(&*cache_dir).join("xkcd.json");
    let xkcd_raw_data = if !Path::exists(&xkcd_path)
        || fs::metadata(&xkcd_path)
            .expect("failed to get metadata for XKCD data file")
            .modified()
            .expect("modified time should be available")
            .elapsed()
            .expect("failed to get elapsed time")
            > Duration::from_secs(3600)
    {
        info!("Fetching XKCD");
        let xkcd_raw_data = XkcdInfo::fetch(&mut client).await;
        fs::write(
            xkcd_path,
            serde_json::to_string(&xkcd_raw_data)
                .expect("failed to serialize weather data to JSON"),
        )
        .expect("failed to write weather data file");
        xkcd_raw_data
    } else {
        let xkcd_raw_data: XkcdInfo = serde_json::from_str(
            &fs::read_to_string(xkcd_path).expect("failed to read weather data"),
        )
        .expect("failed to deserialize weather data");
        xkcd_raw_data
    };
    let xkcd_data = papero::pochoir::lang::serialize_to_value(xkcd_raw_data)
        .expect("failed to serialize weather data to value");

    // Background
    let background_path = Path::new(&*cache_dir).join("background.json");

    let index = {
        let background_path = background_path.clone();
        let notes_dir = notes_dir.clone();

        move |_req, mut stream: Stream| {
            let weather_data = weather_data.clone();
            let xkcd_data = xkcd_data.clone();
            let background_path = background_path.clone();
            let notes_dir = notes_dir.clone();
            let bookmarks = config.bookmarks.clone();
            let logseq_token = config.logseq_token.clone();
            let mut local_client = local_client.clone();

            async move {
                let date_time: DateTime<Local> = Local::now();
                let random_logseq_page = notes::get_random_note(&notes_dir);
                let todos_logseq_page = if let (Some(logseq_port), Some(logseq_token)) = (config.logseq_port, logseq_token) {
                    logseq::get_todos_page(&mut local_client, logseq_port, logseq_token).await.into_value()
                } else {
                    Value::Null
                };

                let mut context = Context::new();
                context.insert(
                    "time",
                    object! {
                        "hour" => date_time.hour(),
                        "minute" => date_time.minute(),
                        "second" => date_time.second(),
                    },
                );
                context.insert("date", date_time.format("%A, %d %B %Y").to_string());
                context.insert("weather", weather_data);
                context.insert("xkcd", xkcd_data);
                context.insert("weather_svg_icon", weather_svg_icon);
                context.insert("weather_city", config.weather_city);
                context.insert("random_logseq_page", random_logseq_page);
                context.insert("todos_logseq_page", todos_logseq_page);
                context.insert("bookmarks", bookmarks);
                context.insert("bg_seed", fs::read_to_string(background_path).map_or(Value::Null, Value::String));

                #[cfg(debug_assertions)]
                let template = Template::new("Dashboard", "index", context);

                #[cfg(not(debug_assertions))]
                let template = Template::with_data("Dashboard", "index", TEMPLATE, context);

                stream.send(template).await?;
                Ok(())
            }
        }
    };

    let set_background = move |req: Request, mut stream: Stream| {
        let background_path = background_path.clone();

        async move {
            #[derive(Serialize, Deserialize)]
            struct SetBackgroundData {
                #[serde(rename = "bg-seed")]
                bg_seed: String,
            }

            let data: SetBackgroundData = papero::serde_urlencoded::from_bytes(req.body()).expect("bad data for set background route"); // TODO: Error management

            if data.bg_seed.is_empty() {
                fs::remove_file(background_path).expect("failed to remove background file");
            } else {
                fs::write(background_path, data.bg_seed).expect("failed to write background file");
            }

            stream
                .send(
                    Builder::new()
                        .status(StatusCode::SEE_OTHER)
                        .header(header::LOCATION, "/")
                        .body(vec![])
                        .expect("failed to build response")
                )
                .await?;
            Ok(())
        }
    };

    let notes_assets = move |req: Request, mut stream: Stream| {
        let notes_dir = notes_dir.clone();

        async move {
            let inner_path = req.uri().path().strip_prefix('/').unwrap();
            let complete_path = Path::new(&notes_dir).join(inner_path);
            info!("Try to get asset from {}", complete_path.display());
            let asset_content = fs::read(&complete_path).expect("failed to read asset"); // TODO: Error handling
            stream
                .send(
                    Builder::new()
                        .header(
                            header::CONTENT_TYPE,
                            mime_guess::from_path(complete_path)
                                .first_raw()
                                .expect("failed to guess asset mime type"),
                        )
                        .body(asset_content)
                        .expect("failed to build response"),
                )
                .await?;
            Ok(())
        }
    };

    let mut encre_css_config = encre_css::Config::from_file("encre-css.toml").unwrap_or_default();
    encre_css_config.extra.add(
        "icons",
        toml! {
            prefix = "i-"
        },
    );

    encre_css_typography::register(&mut encre_css_config);
    encre_css_icons::register(&mut encre_css_config);

    let server_config = Config {
        templates_dir: "src/templates".into(),
        encre_css_config,
    };

    let mut router = Router::new();
    router.insert("/", Handler::new(index)).unwrap();
    router.insert("/set-background", Handler::new(set_background)).unwrap();
    router
        .insert("/assets/*p", Handler::new(notes_assets))
        .unwrap();

    let server = Server::new(&config.server_addr, router, server_config);

    info!("Server listening on {}{}", &config.server_addr, if let Ok(addr) = SocketAddr::from_str(&config.server_addr) {
        Cow::Owned(format!(", use http://localhost:{} to access it locally", addr.port()))
    } else {
        Cow::Borrowed("")
    });
    server.run().await
}
